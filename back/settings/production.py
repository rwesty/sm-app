from .base import *

CORS_ORIGIN_WHITELIST = [
    'https://synthetic-minds.ryanwestdev.com',
    'https://calm-mesa-80165.herokuapp.com',
]
