import graphene
import coins.schema


class Query(coins.schema.Query,
            graphene.ObjectType):
    node = graphene.relay.Node.Field()


schema = graphene.Schema(query=Query)
