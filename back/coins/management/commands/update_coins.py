import os
import json
import locale
import requests
from dateutil import parser
from datetime import datetime
from millify import millify

from django.utils.timezone import make_aware
from django.core.management.base import BaseCommand, CommandError
from django.core.serializers.json import DjangoJSONEncoder, Deserializer

from coins.models import Coin

locale.setlocale(locale.LC_MONETARY, 'en_US.UTF-8')


def formatAsPercent(pct):
    return '{0:.2%}'.format(float(pct))


def formatBillions(num):
    return millify(num, precision=2)


def allTimeHighPercent(price, high):
    try:
        return '{0:.0%}'.format(float(price) / float(high))
    except ZeroDivisionError:
        return None


def transformDataToCoinParams(data):
    return {
        'id': data['id'],
        'symbol': data['symbol'],
        'name': data['name'],
        'logo_url': data['logo_url'],
        'price': locale.currency(float(data['price']), grouping=True),
        'price_timestamp': parser.isoparse(data['price_timestamp']),
        'rank': int(data['rank']) if 'rank' in data else None,
        'day_change_pct': formatAsPercent(data['1d']['price_change_pct']) if '1d' in data else None,
        'week_change_pct': formatAsPercent(data['7d']['price_change_pct']) if '7d' in data else None,
        'month_change_pct': formatAsPercent(data['30d']['price_change_pct']) if '30d' in data else None,
        'year_change_pct': formatAsPercent(data['365d']['price_change_pct']) if '365d' in data else None,
        'volume': formatBillions(data['1d']['volume']) if '1d' in data else None,
        'high': allTimeHighPercent(data['price'], data['high']) if 'price' in data and 'high' in data else None,
        'market_cap': formatBillions(data['market_cap']) if 'market_cap' in data else None,
    }


class Command(BaseCommand):
    help = 'Updates database with latest information from Nomics'

    def handle(self, *args, **options):
        query = {
            'key': os.environ['NOMICS_KEY'],
            'interval': '1d,7d,30d,365d',
            'attributes': 'id,original_symbol,name,logo_url'
        }
        r = requests.get(
            f'{os.environ["NOMICS_URL"]}/currencies/ticker', params=query)

        coinParams = [transformDataToCoinParams(coin) for coin in r.json()]

        for params in coinParams:
            Coin.objects.update_or_create(id=params['id'], defaults=params)

        timestamp = datetime.now().astimezone().replace(microsecond=0)
        print(
            f'Saved {len(coinParams)} coins to database - {timestamp.isoformat()}')
