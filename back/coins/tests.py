from django.test import TestCase
from graphene.test import Client
from server.schema import schema


class CoinGraphQLTest(TestCase):
    fixtures = ['coin.json']

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_get_coins(self):
        client = Client(schema)
        coins = client.execute('''
          query {
              coins(first: 10, orderBy: "rank") {
                  edges {
                      node {
                          id
                          symbol
                          name
                          logoUrl
                          high
                          price
                          priceTimestamp
                          marketCap
                          rank
                          dayChangePct
                          weekChangePct
                          monthChangePct
                          yearChangePct
                          volume
                      }
                  }
              }
          }
        ''')

        # Returns first 10 coins
        self.assertEqual(len(coins['data']['coins']['edges']), 10)

        # First in order is Bitcoin, 1st in rank
        self.assertEqual(coins['data']['coins']['edges']
                         [0]['node']['symbol'], 'BTC')
