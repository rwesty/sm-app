from django.db import models


class Coin(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    symbol = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    logo_url = models.URLField()
    price = models.CharField(max_length=255)
    price_timestamp = models.DateTimeField()
    rank = models.IntegerField(null=True)
    day_change_pct = models.CharField(max_length=255, null=True)
    week_change_pct = models.CharField(max_length=255, null=True)
    month_change_pct = models.CharField(max_length=255, null=True)
    year_change_pct = models.CharField(max_length=255, null=True)
    volume = models.CharField(max_length=255, null=True)
    high = models.CharField(max_length=255, null=True)
    market_cap = models.CharField(max_length=255, null=True)
