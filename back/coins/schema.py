
from dateutil.parser import parse
import graphene
from graphene_django.types import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField
from graphene.utils.str_converters import to_snake_case
from coins.models import Coin


# OrderBy https://stackoverflow.com/a/58101374/8326512
class OrderedDjangoFilterConnectionField(DjangoFilterConnectionField):
    @classmethod
    def resolve_queryset(
        cls, connection, iterable, info, args, filtering_args, filterset_class
    ):
        qs = super(DjangoFilterConnectionField, cls).resolve_queryset(
            connection, iterable, info, args
        )
        filter_kwargs = {k: v for k, v in args.items() if k in filtering_args}
        qs = filterset_class(data=filter_kwargs,
                             queryset=qs, request=info.context).qs

        order = args.get('orderBy', None)
        if order:
            if type(order) is str:
                snake_order = to_snake_case(order)
            else:
                snake_order = [to_snake_case(o) for o in order]
            qs = qs.order_by(*snake_order)
        return qs


class CoinType(DjangoObjectType):
    class Meta:
        model = Coin
        interfaces = [graphene.relay.Node]
        filter_fields = ('rank',)


class CoinConnection(graphene.relay.Connection):
    class Meta:
        node = CoinType


class Query(graphene.ObjectType):
    coins = OrderedDjangoFilterConnectionField(
        CoinType, orderBy=graphene.List(of_type=graphene.String))

    def resolve_coins(root, info, **kwargs):
        return Coin.objects.all()

    def resolve_coin_by_id(root, info, **kwargs):
        id = kwargs.get('id')
        try:
            return Coin.objects.get(id=id)
        except Coin.DoesNotExist:
            return None
