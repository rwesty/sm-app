import os

from django.http import HttpResponse
from django.core.management import call_command


def health(request):
    return HttpResponse(status=200)


def update_coins(request):
    if request.method != 'POST':
        return HttpResponse(status=404)

    if request.headers.get('Authorization') != f'Bearer: {os.environ["COIN_SECRET"]}':
        return HttpResponse(status=400)

    call_command('update_coins')
    return HttpResponse(status=200)
