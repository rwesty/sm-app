from django.apps import AppConfig


class CoinAppConfig(AppConfig):
    name = 'coins'
