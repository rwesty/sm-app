//import superagent from 'superagent';

/* const request = superagent.agent().use((req) => {
  req.url = process.env.REACT_APP_API_URL + req.url;
  return req;
}); */

export type Coin = {
  id: string;
  symbol: string;
  name: string;
  logoUrl: string;
  price: string;
  priceTimestamp: string;
  rank: string;
  dayChangePct?: string;
  weekChangePct: string;
  monthChangePct: string;
  yearChangePct: string;
  volume: string;
  high: string;
  marketCap: string;
};

export const getCoins = async (): Promise<Coin[]> => {
  //const { body } = await request.get('/coins');
  //return body;

  return new Promise((resolve, reject) =>
    resolve([
      {
        id: 'BTC',
        symbol: 'BTC',
        name: 'Bitcoin',
        logoUrl:
          'https://s3.us-east-2.amazonaws.com/nomics-api/static/images/currencies/btc.svg',
        high: '0.61',
        price: '11853.77542316',
        priceTimestamp: '2020-08-21T05:44:00Z',
        marketCap: '218898632885',
        rank: '1',
        weekChangePct: '0.0157',
        monthChangePct: '0.2005',
        yearChangePct: '0.1649',
        volume: '8099657022331.93',
      },
      {
        id: 'ETH',
        symbol: 'ETH',
        name: 'Ethereum',
        logoUrl:
          'https://s3.us-east-2.amazonaws.com/nomics-api/static/images/currencies/eth.svg',
        high: '0.61',
        price: '11853.77542316',
        priceTimestamp: '2020-08-21T05:44:00Z',
        marketCap: '218898632885',
        rank: '1',
        weekChangePct: '0.0157',
        monthChangePct: '0.2005',
        yearChangePct: '0.1649',
        volume: '8099657022331.93',
      },
      {
        id: 'XRP',
        symbol: 'XRP',
        name: 'Ripple',
        logoUrl:
          'https://s3.us-east-2.amazonaws.com/nomics-api/static/images/currencies/XRP.svg',
        high: '0.61',
        price: '11853.77542316',
        priceTimestamp: '2020-08-21T05:44:00Z',
        marketCap: '218898632885',
        rank: '1',
        weekChangePct: '0.0157',
        monthChangePct: '0.2005',
        yearChangePct: '0.1649',
        volume: '8099657022331.93',
      },
    ])
  );
};
