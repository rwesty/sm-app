# Synthetic Minds Demo App

Hello, I created this app to demonstrate a base knowledge in Django, PostgresSQL, GraphQL, React and Typescript. Boilerplate code was used extensivley and the app is deployed to Heroku with a docker container.

I created a simple view to list the latest prices of Cryptocurrencies as a bite-sized project to demonstrate some knowledge beyond framework boilerplate code.

Thank you for considering!

## Running locally

```sh
git clone git clone https://rwesty@bitbucket.org/rwesty/sm-app.git

## Frontend
cd front && yarn && yarn start

## Backend
cd back && ./scripts/start-container
```

## Deploy

```sh
./scripts/deploy
```

## Resources

Django, Postgres, GraphQL Boilerplate - https://github.com/rwest202/django-postgres-graphql-boilerplate

Create React App - https://create-react-app.dev
